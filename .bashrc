# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
        . /etc/bashrc
fi

# User specific environment
if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:" ]]
then
    PATH="$HOME/.local/bin:$HOME/bin:$PATH"
fi
export PATH

# Don't put duplicate lines in the history. See bash(1) for more options
# don't overwrite GNU Midnight Commander's setting of `ignorespace'.
HISTCONTROL=$HISTCONTROL${HISTCONTROL+,}ignoredups
# ... or force ignoredups and ignorespace
HISTCONTROL=ignoreboth

# Append to the history file, don't overwrite it
shopt -s histappend

# For setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions
alias ll='ls -l'
alias la='ls -A'
alias l='ls -CF'
alias lh='ll -h'

alias rm='rm -i'
alias cp='cp -i'
alias cd..='cd ..'
alias vi='vim'

# Editeur par defaut
export EDITOR=/usr/bin/vim

# Evite que le nom du terminal se renomme dans screen
unset PROMPT_COMMAND
alias docker='podman'

. $HOME/.bash_aliases

ulimit -c unlimited

export LC_MESSAGES=en_US.UTF-8
#export https_proxy="http://proxy1.si.c-s.fr:3128"

# Persist the Kubeconfig Context
export KUBECONFIG=$KUBECONFIG:$HOME/.kube/config:$HOME/.kube/configs/kubeconfig.yaml
