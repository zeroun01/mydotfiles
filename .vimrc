" Vim configuration : vimrc file.

" Use Vim settings, rather then Vi settings (much better!).
" This must be first, because it changes other options as a side effect.
set nocompatible

" Switch syntax highlighting on
syntax on

" Make shift-insert work like in Xterm
map <S-Insert> <MiddleMouse>
map! <S-Insert> <MiddleMouse>

set ruler                           " Affiche la position du curseur
set backspace=indent,eol,start      " Allow backspacing over everything in insert mode
set hlsearch                        " Switch on highlighting the last used search pattern
set history=50                      " Garder 50 lignes d'historique des commandes
set noshowcmd                       " Display incomplete commands
set incsearch                       " Recherche incrementale
set paste                           " Coller sans autoindentation
set nobackup                        " Ne pas cree un fichier *~ de backup
set background=dark
set tabstop=4 shiftwidth=4 expandtab " Les tabs sont remplaces par X espaces
"set mouse=a                         " Activer la souris (all modes)

"Afficher les caracteres invisibles
"set list listchars=nbsp:¤,tab:>-,trail:¤,extends:>,precedes:<,eol:¶,trail:·

" Vim jump to the last position when reopening a file
if has("autocmd")
    au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif

" Don't use Ex mode, use Q for formatting
map Q gq

" Only do this part when compiled with support for autocommands.
if has("autocmd")
  " Enable file type detection.
  " Use the default filetype settings, so that mail gets 'tw' set to 72,
  " 'cindent' is on in C files, etc.
  " Also load indent files, to automatically do language-dependent indenting.
  filetype plugin indent off

  " Put these in an autocmd group, so that we can delete them easily.
  augroup vimrcEx
  au!

  " For all text files set 'textwidth' to 78 characters.
  autocmd FileType text setlocal textwidth=78

  " When editing a file, always jump to the last known cursor position.
  " Don't do it when the position is invalid or when inside an event handler
  " (happens when dropping a file on gvim).
  autocmd BufReadPost *
    \ if line("'\"") > 0 && line("'\"") <= line("$") |
    \   exe "normal! g`\"" |
    \ endif

  augroup END
else
  set noautoindent        " always set autoindenting off
endif " has("autocmd")

" Surligner les espaces en fin de ligne et les tabulations
highlight RedundantSpaces ctermbg=red guibg=red
match RedundantSpaces /\s\+$\| \+ze\t\|\t/

set ruler                 " Affiche la position du curseur

" Highlight cursor line underneath the cursor horizontally.
set cursorline

" Highlight cursor line underneath the cursor vertically.
" set cursorcolumn

" Ignore capital letters during search.
set ignorecase

" Override the ignorecase option if searching for capital letters.
" This will allow you to search specifically for capital letters.
set smartcase


set cc=72
