urxvtc() {
    urxvtc "$@"
    if [ $? -eq 2 ]; then
        urxvtd -q -o -f
        urxvtc "$@"
    fi
}
alias k='kubectl'

alias vsoip="cd $HOME/Repos/vsoip_src/"

alias conf="cd $HOME/Repos/vsoip_src/src-conference"
export VSOIP_SRC="$HOME/Repos/vsoip_src/"

alias run="cd $HOME/Repos/vsoip_src/build/bin; ./tests-RngConference; cd -"

alias build="cd $HOME/Repos/vsoip_src/build/; make -j7; cd -;"

# Mise à jour de tous les depots vsoip
vsoip_update() {
    pwd=$PWD
    cd $HOME/Repos/vsoip_src
    for dir in . src-ua src-ua_mixer src-xml src-conference ng_src-fmk
    do
        cd $dir
        echo "### $PWD ###"
        git switch master
        git pull --recurse-submodules
        git switch -
        cd -
        echo "########################################"
        echo
    done
    cd $pwd
}

# changer la branche sur tous les depots vsoip
vsoip_switch() {                                                        
    pwd=$PWD                                                            
    cd $HOME/Repos/vsoip_src                                            
    for dir in . src-ua src-ua_mixer src-xml src-conference ng_src-fmk  
    do                                                                  
        cd $dir                                                         
        echo "### $PWD ###"                                             
        git switch $1                                               
        cd -                                                            
        echo "########################################"                 
        echo                                                            
    done                                                                
    cd $pwd                                                             
}

sdd_update() {
    pwd=$PWD
    cd $HOME/workspace-papyrus
    for dir in ng_idd ng_sdd-fmk sdd-xml sdd-conference sdd-ua sdd-ua_mixer vsoip_sdd
    do
        cd $dir
        echo "### $PWD ###"
        git switch master
        git pull
        git switch -
        cd -
        echo "########################################"
        echo
    done
    cd $pwd
}

podman_clean() {
    podman kill --all
    podman rm --force --all
    podman pod rm --force --all
}

alias papyrus="GTK_THEME=Adwaita:light /home/mghour_x/Downloads/Papyrus-2020-03-4.7.0/papyrus &"

